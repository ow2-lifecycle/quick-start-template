# OW2 Quick Start Template

This project is a quick start template aiming at facilitating the set up of Java OW2 projects in GitLab.

It covers the following steps:
* A hello-ow2 application together with a Maven POM file for compiling, testing and packaging the application
* A YAML file to execute a Maven local cache in order to avoid the download of all dependencies on each compilation
* The publication of artifacts into OW2 Nexus
* The execution of SonarQube on the application code
* The distribution of the application
* An example of extreme mutation testing based on STAMP / Descartes tool

It includes a .gitignore file ready for IDEs (eg. "mvn eclipse:eclipse" would not trigger commit of inappropriate files).

## Hello-OW2 application

This application follows the template provided by Maven at [Maven in 5 minutes](http://maven.apache.org/guides/getting-started/maven-in-five-minutes.html). It consists of a Java class and of a unit test.

## CI's YAML configuration file overview

The [project's CI YAML file](.gitlab-ci.yml) is named by convention `.gitlab-ci.yml`. There is a [detailed documentation](http://docs.gitlab.com/ce/ci/yaml/README.html) at GitLab.com about how to use it.

[.gitlab-ci.yml](.gitlab-ci.yml) we have here is inspired by the `Maven` [YAML template provided by GitLab](https://gitlab.com/gitlab-org/gitlab-ci-yml/blob/master/Maven.gitlab-ci.yml).

Broadly speaking, the configuration defines jobs and stages, by specifying a Docker image with the commands to run into the container in order to build the project.

We also define a "sonar" stage to publish source code analysis to https://sonarqube.ow2.org/

### Maven related settings for Sonar

The virtual machine that runs the [GitLab Runner](https://gitlab.com/gitlab-org/gitlab-ci-multi-runner) have a `settings.xml` mounted to `/root/.m2/settings.xml` in the spawned containers (for any image). Maven reads this file by default, unless you specify `--settings` to mvn CLI command.

## runners : specific, shared and tags

At the time being, there is one shared runner that can run untagged jobs.
a Shared runner is common to the whole GitLab instance and can be used by any project.

## Publication of snapshots artifacts into OW2 Nexus

The `deploy` stage make use of a user specified [settings.xml](settings.xml) where Nexus credentials are stored in GitLab [Secret Variables](https://docs.gitlab.com/ce/ci/variables/#secret-variables).

Instead of using the user's login and password in a clear former, we use [Nexus's *User Token* feature](https://books.sonatype.com/nexus-book/reference/usertoken.html). To be able generate and use tokens, *the Nexus user must have the role "User Token: Basic" (which contains the privilege "User Token: Current"), to be able to access the token.*

## STAMP/Descartes mutation testing

First step: build / test the project ( `mvn clean install`).

Then, to experiment mutation testing:
`mvn eu.stamp-project:pitmp-maven-plugin:descartes`

The output is located under /tmp/pit-reports (as specified in pom configuration). See index.html file in subdirectory there (subdir name is the timestamp of the test: eg. /tmp/pit-reports/202001311149).

Comments are included in the code under src/main/java and src/test/java, to describe what is detected by the mutation tests.

The POM configuration also provides a sample configuration to generate Gitlab issues automatically, when mutation testing detects critical pseudo-tests: adapt it to your project when needed.

More details here, about how to [generate Gitlab issues out of mutation testing](https://gitlab.ow2.org/ow2-lifecycle/ow2-stamp-descartes-git).

## Distribution of the application

This step is a work in progress.

## Generation of a Docker image

This step is a work in progress.

## Publication of a Docker image in the GitLab Docker registry

This step is a work in progress.
