package org.ow2.template_app;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) {
        (new App()).sayHello();
    }
    
    /**
     * Say hello (useful to showcase Pitest/Descartes mutation testing tool:
     * see corresponding test in AppTest.java)
     * @return true
     */
    public boolean sayHello() {
    	System.out.println("Hello World!");
    	return true;
    }
}
