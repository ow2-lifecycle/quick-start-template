package org.ow2.template_app;

import org.junit.Test;

import junit.framework.TestCase;

/**
 * Unit test for simple App.
 */
public class AppTest extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName ) {
        super( testName );
    }

    /**
     * Warning: this is a pseudo-test !
     * if sayHello() method body is replaced by "return true;", test passes...
     * The issue aims at being detected by Pitest/Descartes mutation testing tool.
     * Process:
     * - mvn clean install
     * - mvn eu.stamp-project:pitmp-maven-plugin:descartes
     * Then see reports in /tmp/pit-reports
     */
    @Test
    public void testSayHello() {
    	App app = new App();
    	assertTrue(app.sayHello());
    }
}
